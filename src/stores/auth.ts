import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
    id: 1,
    email: 'kim123@gmail.com',
    password: 'pass@1234',
    fullName: 'kim Doyoung',
    gender: 'male',
    roles: ['user']
  })
  return { currentUser }
})