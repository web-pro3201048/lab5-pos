import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { isTemplateExpression } from 'typescript'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    {id: 1, name: 'lee Haechan', tel: '0971366497'},
    {id: 2, name: 'na Jaemin', tel: '0611451569'}
  ])
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0 ) {
        currentMember.value = null
    }
        currentMember.value = members.value[index]
  }

  function clear(){
    currentMember.value = null
  }


  return { members , currentMember,
            searchMember , clear
    }
})
